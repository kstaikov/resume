To create a pdf of the resume:

    pandoc -f markdown -t latex -o resume.pdf resume.md -V geometry:"top=1.25cm, bottom=1.5cm, left=1cm, right=1.5cm" -V urlcolor=blue

