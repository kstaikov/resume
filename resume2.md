Kiril Staikov | <kstaikov@gmail.com> | Atlanta, GA
==================================================
Hands-on and highly engaged leader with a uniquely varied background of
technical and people skills. I can lead globally distributed teams through all
aspects of software development, ranging from even HR to product management.
Experience in startup, enterprise, and academic environments in widely varying
fields (move this up?)

Highlights (Kill this?)
----------
* Managing a $5M+ and ~80-person international, remote org.
* Reduced cost of a traveller-facing product from a peak of ~$12M/yr to ~$2M/yr
  by moving all development and operations in house.
* Launched two brand new verticals (air and car rental
  shopping/booking/servicing) and in the process of adding a third (rail) to
  what was originally a hotels-only product.
* Triggered a new experimental methodology and direction in a neuro lab by
  creating a 3D virtual navigation game and training monkeys to play.

Work Experience
---------------

__Sr. Director of Engineering__ | __Technical Product Owner__ | __Sr. Software Engineer__ | BCD Travel | _2017 - Present_

* Responsible for a ~$5M budget and a globally distributed, remote
  organization of ~80-people.
* Departments in my purview include development, QA, technical product
  management, ops, technical support, and HR. Internally referred to as
  "running a company inside the company".
* My team builds several modern mission-critical APIs and web applications for
  BCD partners, travel agents, and travellers that handle millions of
  booking transactions a year.
* Managing a team mostly based in Ukraine through the war, making decisions in
  situations for which no standard playbook exists.
* Steadily moved reliance on overly costly third-party development for our
  traveller-facing product (~$12M/yr at peak) inhouse (~$2M/yr), resulting in a
  more affordable, better-performing, and sustainable model and product.
* Launched two brand new verticals (air and car rental
  shopping/booking/servicing) and in the process of adding a third (rail) to
  what was originally a hotels-only product.
* Designed and refined team roles, responsibilities, and processes. Created and
  instituted performance review process.
* Maintained an average employee tenure of 3.5 years in Ukraine during a
  multi-year unprecedentedly hot IT  market with an average tenure rate of 15
  months.
* Systematically refined and overhauled tedious and ineffective, approach to
  manual regression and automated testing towards a new, more holistic
  approach. Over 6 months manual regression time has progressively been
  shrinking from over a week to currently less than 2 days.
* Started a book club and tech talk series to foster a mutually learning
  environment.

__Software Engineer__ | Kabbage, Inc. | _December 2014 - March 2017_

*   Full-stack engineer on a greenfield project in a startup: a customer
    servicing platform for Kabbage loans. It was used by auditors, customer
    service, sales, underwriting, and collections agents.
*   Received 3 "Thinking Right Awards": recognition for "going above and beyond
    the call of duty".

__Python Developer__ | The Weather Channel | _July 2013 - December 2014_

I wrote code for machines at cable headends across the country that take in
satellite weather data, create visualizations of local weather data, and place
them over the national video feed. My work is/was on The Weather Channel every
day: The _"Local on the 8's"_, the ticker on the bottom of the screen, and many
of the sidebars.

__Research Specialist__ | Elizabeth Buffalo's Lab | Emory University | _March 2010 - July 2013_

*   Performed various electorphysiological neuro experiments with monkeys.
*   Turned a joke into a wholly new methodology and direction for the
    lab by creating a custom 3D "virtual reality" game for monkeys and
    interfacing it with various hardware for treat distribution, eye
    tracking, and electrophysiological recroding.
    Video: [__Monkey Bananarchy__](http://tinyurl.com/MonkeyBananarchy)
*   Introduced version control for all lab code.
*   Mentored and managed high school and undergraduate lab members/interns.
*   Created lab website.

__Research Assistant__ | Josh Gulley's Lab | University of Illinois at Urbana-Champaign | _March 2008 - August 2009_

Performed various electorphysiological neuro experiments with rats,
exploring individual differences in responses to amphetamines.

Education
---------

* _Emory University_, Non-Degree-Seeking Student, Comp. Sci. Bioinformatics courses, 2011-2012
* _University of Illinois at Urbana-Champaign_, Bachelor's in Neuroscience and Philosophy, May 2009

Select Personal Projects
------------------------
*   __Decatur Bicycle Coalition, Board of Directors__ | _2019 - Present_

      Working with the local government, community, and schools to ensure
      walking and cycling are fostered and encouraged, and that the needs of
      these modes of transportation are always taken into account in municipal
      decisions.

*   __HOA Community Communication Tools__ | _2017 - Present_

      My community of 60 townhomes was having a hard time staying connected
      with each other and there was no good solution available. While serving
      on the HOA board, I stood up an instance of the open-source Discourse
      software, landed pull requests for some features I needed and wrote some
      custom plugins for others, and soon had a nice townhome community
      website, forum, and mailing list all in one easy-to-use package. All 60
      households are on it and love it, and messages/posts are going up almost
      daily for the last 5 years.

*   __Rental Housing Securitization Analysis__ | _Early 2014 - Late 2017_

      Computational journalism project tracking home single-family home
      purchases and subsequent rental listings and trends by three big private
      equity firms on a massive scale across the USA.

*   __Technology Committee Chair, Board of Directors__ | Oakhurst Cooperative Preschool | _August 2014 - May 2016_

      Managed all technology help requests, class mailing lists, rosters,
      parent contact information, website, etc. School management consisted of
      a barely patched together and always-breaking system of Google Docs
      spreadsheets, Google Groups, Google Sites, and custom scripts. Migrated
      the school to a much better third-party solution for our idiosyncratic
      preschool. Parents and staff were thrilled.

*   __Guillotuned__ | _Late 2013 - Late 2014_

      Headless streaming web radio station, developed and run with a group of
      friends to alleviate commuting woes.
