Kiril Staikov | <kstaikov@gmail.com> | [LinkedIn](https://www.linkedin.com/in/kiril-staikov-4089b79/) | Atlanta, GA
===================================================================================================================

Education
---------

* _Emory University_, Non-Degree-Seeking Student, Comp. Sci. Bioinformatics courses, 2011-2012
* _University of Illinois at Urbana-Champaign_, Bachelor's in Neuroscience and Philosophy, May 2009

Work Experience
---------------

__Sr. Director of Engineering__ | __Technical Product Owner__ | __Sr. Software Engineer__ | BCD Travel | _2017 - Present_

Lead a remote, globally distributed cross-functional group of ~80 people, with
many located in a warzone. It spans dev, QA, technical product management,
technical support, devops, and even an HR department. What I do is internally
referred to as "running a company inside the company". My team builds modern
mission-critical APIs and web applications for BCD partners, travel agents, and
travellers that handle millions of transactions a year.

In my time here I:

* Lead the research, creation, and launch of two brand new verticals (air and car rental
  shopping/booking/servicing).
* Steadily moved reliance on overly costly third-party development in-house, resulting
  in a more affordable, better-performing, and sustainable model.
* Designed and refined team roles, responsibilities, and processes.
* Became subject-matter expert on technical and business aspects of Global
  Distribution Systems, NDC, and travel industry technology in general.
* Made hard decisions in situations for which there is no standard playbook -
  where deep humanitarian ethics, geopolitics, business sense, and typical
  company policy did not necessarily all align with each other.
* Nurtured talent in various domains through role expansion and leadership growth.
* Worked as an engineer with Python, Docker, Redis, Postgres, MongoDB, travel
  Global Distribution Systems, and more.

__Software Engineer__ | Kabbage, Inc. | _December 2014 - March 2017_

*   Worked on a small team to build a customer servicing platform for Kabbage loans, Kore.
*   Deeply involved in all layers and stages of the product from inception.
*   Technologies: Docker, Python, Django/REST-Framework, AngularJS, Pytest, Protractor, Selenium.
*   Our team and product always received stellar reviews from all of our
    stakeholders, which included auditors, customer service, sales,
    underwriting, collections.
*   Received 3 "Thinking Right Awards": recognition for "going above and beyond the call of duty".

__Python Developer__ | The Weather Channel | _July 2013 - December 2014_

*   Worked on [__IntelliStar__](http://en.wikipedia.org/wiki/IntelliStar) systems that are shipped
    to cable headends across the country to process the specific locale's weather data, produce
    graphical video presentations of the data, and povide our headquarters with the ability to
    finely control a geographical area's viewing experience.
*   Implemented new design and overhaul of The Weather Channel's entire collection of localized TV
    Weather Products. These included those that constitute the _"Local on the 8's"_
    (a one-minute presentation of your local weather every 10 minutes) and the constant Lower
    Display Line, among others. Sample video of some products:
    [__Weather Channel Relaunch__](http://www.tinyurl.com/NewWeatherChan) - Developed every featured
    product for the Standard Definition systems.
*   Mostly wrote Python code that interfaced with an OpenGL-based in-house rendering engine to
    create the scores of visual products (Now, Tonight, Hourly Forecast, Radars, etc.). Often this
    involved making C++ modifications and extensions to the rendering engine to impliment new
    graphical effects requested by design.
*   Developed entirely novel products for presenting local traffic conditions. Also worked on
    architecture and development of my team's side of the traffic data feed itself and the main
    algorithm for processing and prioritizing traffic incidents.
*   Rewrote and templetized existing system for creating advertisements that present (and are even
    triggered by) the viewer's local weather conditions. Currently working on leveraging this code
    into a new all-encompassing ad platform.
*   Implemented designs for an "Alternative Network Feed" option on Intellistars, whereby
    meteorologists can personally select areas experiencing severe weather and preempt regularly
    scheduled programming with a specialized live broadcast only to be seen by the given area.
*   Wrote many scripts (in bash, Python, and Powershell), to streamline various conversions of lookup
    tables and weather data, perform batch image manipulation, test and analyze data feeds, look for
    anomalous data, etc.
*   Provided continuous production support for systems out in the field after releases.
*   Developed mostly on UNIX, FreeBSD boxes. At other times worked in Windows Server 2008, XP
    Embedded and 7 Embedded.

__Research Specialist__ | Elizabeth Buffalo's Lab | Emory University | _March 2010 - July 2013_

*   Created custom 3D virtual reality program for monkeys. Designed 3D models in Maya and wrote
    Python code utilizing Panda3D game engine and National Instruments Data Acquisition (NI-DAQ)
    driver APIs to control gameplay, monitor eye-gaze, deliver food reward, and sync virtual
    behavioral data with neural electrophysiological data. Created training steps and trained the
    monkeys to perform goal-seeking behavior in virtual space. Sample Video:
    [__Monkey Bananarchy__](http://tinyurl.com/MonkeyBananarchy)
*   Programmed other behavioral tasks in a variety of softwares and languages: Presentation (Python
    and custom C and BASIC-based scripting languages), MonkeyLogic (MATLAB-based), CORTEX (C-based).
*   Hardware and minor electrical support: Created custom cables to connect various hardware,
    ensured appropriate terminal board wiring, wrote code utilizing NI-DAQ API to perform
    appropriate data acquisition functions and interface between different software and hardware.
*   Analyzed behavioral performance and eye-tracking data using MATLAB.
*   Implemented version control for the lab's code. Created and managed git repository, enabling
    each lab member to easily access and take advantage of code previously written by other
    team members.
*   Designed, implemented, and managed lab website.
*   Performed basic lab and animal maintenance and training. Mentored a high school student through
    a full-time summer internship in the lab and the completion of all aspects of a behavioral
    experiment. Trained undergraduates in lab procedures.
*   Managed the lab when usual lab manager was unavailable.

__Research Assistant__ | Josh Gulley's Lab | University of Illinois at Urbana-Champaign | _March 2008 - August 2009_

*   Conducted longitudinal behavioral and electrophysiological studies exploring individual
    differences in amphetamine-treated adolescent and adult rats.
*   Designed, programmed, and conducted daily operant chamber tasks.
*   Designed, constructed, and stereotaxically implanted microwire electrodes in rat brains.
*   Extracted and analyzed behavioral and electrophysiological data.
*   Presented and led discussions on various scientific papers.

Personal Tech Projects
----------------------

*   __HOA Community Communication Tools__ | _2017 - Present_

      My community of 60 townhomes was having a hard time staying connected
      with each other and there was no good solution available. During my time
      on the HOA board, I stood up an instance of the open-source Discourse
      software, landed pull requests for some features I needed  and wrote some
      custom plugins for others, and soon had a nice townhome community
      website, forum, and mailing list all in one easy-to-use package. All 60
      households are on it and love it, and messages/posts are going up almost
      daily for the last 5 years.

*   __Rental Housing Securitization Analysis__ | _Early 2014 - Late 2017_

      This exercise in computational journalism was an exploration of the
      ongoing process of single-family home acquisition, property management,
      and subsequent rental securitization as currently practiced by a few
      private equity firms on a massive scale across the USA. I wrote software
      that continuously scraped the available listings from the websites of
      each firm's property management company. Each snapshot of data for every
      listed home was stored in a database and tracked over time. Analysis was
      then performed on the captured data and cross-referenced with other
      public records sources (census, property records, etc.)  I sought to
      answer questions about the viability of the business model and to
      understand its effects on communities, demographics, property management,
      and investors.  Involved lots of bash scripts, Python / Pandas, and
      general webscraping.

*   __Technology Committee Chair, Board of Directors__ | Oakhurst Cooperative Preschool | _August 2014 - May 2016_

      Managed all technology help requests, class mailing lists, rosters,
      parent contact information, website, etc. School management consisted of
      a barely patched together and always-breaking system of Google Docs
      spreadsheets, Google Groups, Google Sites, and custom scripts. Migrated
      the school to a much better third-party solution for our idiosyncratic
      preschool. All parents and staff have been thrilled.

*   __Guillotuned__ | _Late 2013 - Late 2014_

      Headless streaming web radio station with friends. A daemon that
      piggybacked on Music Player Daemon (MPD). It was always playing the
      latest episodes from RSS feeds of select podcasts and news sites,
      interspersed with files from a shared music directory. We used it with a
      group of friends as a common radio station with an eternally
      self-managing and self-refreshing playlist to alleviate commuting woes.
